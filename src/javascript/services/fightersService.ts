import { callApi } from '../helpers/apiHelper';
import { Fighter, FighterDetails } from '../helpers/interfaces';

class FighterService {
  async getFighters(): Promise<Fighter[]> {
    try {
      const endpoint: string = 'fighters.json';
      const apiResult: Fighter[] = (await callApi(endpoint, 'GET')) as Fighter[];

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string): Promise<FighterDetails> {
    try {
      const endpoint: string = `details/fighter/${id}.json`;
      const apiResult: FighterDetails = (await callApi(endpoint, 'GET')) as FighterDetails;

      return apiResult;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService: FighterService = new FighterService();
