import { controls } from '../../constants/controls';
import { setFighterHealthBar, playSound, showIconHit } from './fightEffects';
import { FighterDetails, Player } from '../helpers/interfaces';
import { IntervalCriticalHit } from '../../constants/time';

export async function fight(firstFighter: FighterDetails, secondFighter: FighterDetails): Promise<FighterDetails> {
  return new Promise((resolve) => {
    const fighterOne: Player = createPlayer(firstFighter, 'left');
    const fighterTwo: Player = createPlayer(secondFighter, 'right');
    const pressedKeys: Set<string> = new Set();

    document.addEventListener('keydown', keydownHandler);
    document.addEventListener('keyup', keyupHandler);

    function keydownHandler(event: KeyboardEvent): void {
      pressedKeys.add(event.code);

      switch (event.code) {
        case controls.PlayerOneAttack:
          if (!pressedKeys.has(controls.PlayerOneBlock) && !pressedKeys.has(controls.PlayerTwoBlock)) {
            makeHit(fighterOne, fighterTwo);
          }
          if (pressedKeys.has(controls.PlayerTwoBlock)) {
            playSound('block');
            showIconHit(fighterTwo, 'block');
          }
          break;
        case controls.PlayerTwoAttack:
          if (!pressedKeys.has(controls.PlayerTwoBlock) && !pressedKeys.has(controls.PlayerOneBlock)) {
            makeHit(fighterTwo, fighterOne);
          }
          if (pressedKeys.has(controls.PlayerOneBlock)) {
            playSound('block');
            showIconHit(fighterOne, 'block');
          }
          break;
      }

      if (
        !fighterOne.blockCriticalHit &&
        controls.PlayerOneCriticalHitCombination.includes(event.code) &&
        checkKeysCriticalHit(controls.PlayerOneCriticalHitCombination, pressedKeys)
      ) {
        makeCriticalHit(fighterOne, fighterTwo);
      }
      if (
        !fighterTwo.blockCriticalHit &&
        controls.PlayerTwoCriticalHitCombination.includes(event.code) &&
        checkKeysCriticalHit(controls.PlayerTwoCriticalHitCombination, pressedKeys)
      ) {
        makeCriticalHit(fighterTwo, fighterOne);
      }
    }

    function keyupHandler(event: KeyboardEvent): void {
      pressedKeys.delete(event.code);
    }

    function makeHit(attacker: Player, defender: Player): void {
      showEffects(defender, 'hit');
      const damage: number = getDamage(attacker.fighter, defender.fighter);
      setFighterHealt(attacker, defender, damage);
    }

    function makeCriticalHit(attacker: Player, defender: Player): void {
      showEffects(defender, 'crit');
      const damage: number = getCriticalDamage(attacker.fighter);
      attacker.blockCriticalHit = true;
      setTimeout((): void => {
        attacker.blockCriticalHit = false;
      }, IntervalCriticalHit);
      setFighterHealt(attacker, defender, damage);
    }

    function showEffects(defender: Player, typeHit: 'hit' | 'crit'): void {
      playSound(typeHit);
      showIconHit(defender, typeHit);
    }

    function setFighterHealt(attacker: Player, defender: Player, damage: number): void {
      defender.health -= damage;
      setFighterHealthBar(defender);
      if (defender.health <= 0) resolve(attacker.fighter);
    }
  });
}

function createPlayer(fighter: FighterDetails, position: 'right' | 'left'): Player {
  return {
    fighter: fighter,
    health: fighter.health,
    blockCriticalHit: false,
    indicator: document.getElementById(`${position}-fighter-indicator`) as HTMLDivElement,
  };
}

export function getDamage(attacker: FighterDetails, defender: FighterDetails): number {
  const hitPower: number = getHitPower(attacker);
  const blockPower: number = getBlockPower(defender);
  return Math.max(0, hitPower - blockPower);
}

export function getCriticalDamage(attacker: FighterDetails): number {
  return attacker.attack * 2;
}

export function getHitPower(fighter: FighterDetails): number {
  const criticalHitChance: number = Math.random() + 1;
  const power: number = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter: FighterDetails): number {
  const dodgeChance: number = Math.random() + 1;
  const power: number = fighter.defense * dodgeChance;
  return power;
}

function checkKeysCriticalHit(keys: string[], pressed: Set<string>): boolean {
  return keys.every((code) => pressed.has(code));
}
