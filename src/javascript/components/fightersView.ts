import { createElement } from '../helpers/domHelper';
import { createFightersSelector } from './fighterSelector';
import { Fighter } from '../helpers/interfaces';

export function createFighters(fighters: Fighter[]): HTMLDivElement {
  const selectFighter: (event: Event, fighterId: string) => Promise<void> = createFightersSelector();
  const container: HTMLDivElement = createElement({
    tagName: 'div',
    className: 'fighters___root',
  }) as HTMLDivElement;
  const preview: HTMLDivElement = createElement({
    tagName: 'div',
    className: 'preview-container___root',
  }) as HTMLDivElement;
  const fightersList: HTMLDivElement = createElement({
    tagName: 'div',
    className: 'fighters___list',
  }) as HTMLDivElement;
  const fighterElements: HTMLDivElement[] = fighters.map((fighter) => createFighter(fighter, selectFighter));

  fightersList.append(...fighterElements);
  container.append(preview, fightersList);

  return container;
}

function createFighter(
  fighter: Fighter,
  selectFighter: (event: Event, fighterId: string) => Promise<void>
): HTMLDivElement {
  const fighterElement: HTMLDivElement = createElement({
    tagName: 'div',
    className: 'fighters___fighter',
  }) as HTMLDivElement;
  const imageElement: HTMLImageElement = createImage(fighter);
  const onClick: (event: Event) => Promise<void> = (event: Event) => selectFighter(event, fighter._id);

  fighterElement.append(imageElement);
  fighterElement.addEventListener('click', onClick, false);

  return fighterElement;
}

function createImage(fighter: Fighter): HTMLImageElement {
  const attributes: { [key: string]: string } = {
    src: fighter.source,
    title: fighter.name,
    alt: fighter.name,
  };
  const imgElement: HTMLImageElement = createElement({
    tagName: 'img',
    className: 'fighter___fighter-image',
    attributes,
  }) as HTMLImageElement;

  return imgElement;
}
