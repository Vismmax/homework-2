import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
import versusImg from '../../../resources/versus.png';
import { createFighterPreview } from './fighterPreview';
import { fighterService } from '../services/fightersService';
import { FighterDetails } from '../helpers/interfaces';

type SelectedFighters = [FighterDetails | false, FighterDetails | false];

export function createFightersSelector(): (event: Event, fighterId: string) => Promise<void> {
  let selectedFighters: SelectedFighters = [false, false];

  return async (event: Event, fighterId: string): Promise<void> => {
    const fighter = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter = playerOne || fighter;
    const secondFighter = Boolean(playerOne) ? playerTwo || fighter : playerTwo;
    selectedFighters = [firstFighter, secondFighter];

    renderSelectedFighters(selectedFighters);
  };
}

const fighterDetailsMap: Map<string, FighterDetails> = new Map();

export async function getFighterInfo(fighterId: string): Promise<FighterDetails | false> {
  try {
    let finger: FighterDetails | undefined = fighterDetailsMap.get(fighterId);
    if (!finger) {
      finger = await fighterService.getFighterDetails(fighterId);
      fighterDetailsMap.set(fighterId, finger);
    }
    return finger;
  } catch (error) {
    return false;
  }
}

function renderSelectedFighters(selectedFighters: SelectedFighters): void {
  const fightersPreview: HTMLDivElement = document.querySelector('.preview-container___root') as HTMLDivElement;
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview: HTMLDivElement = createFighterPreview(playerOne, 'left');
  const secondPreview: HTMLDivElement = createFighterPreview(playerTwo, 'right');
  const versusBlock: HTMLDivElement = createVersusBlock(selectedFighters);

  fightersPreview.innerHTML = '';
  fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: SelectedFighters): HTMLDivElement {
  const canStartFight: boolean = selectedFighters.filter(Boolean).length === 2;
  const onClick: () => void = () => startFight(selectedFighters as [FighterDetails, FighterDetails]);
  const container: HTMLDivElement = createElement({
    tagName: 'div',
    className: 'preview-container___versus-block',
  }) as HTMLDivElement;
  const image: HTMLImageElement = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: versusImg },
  }) as HTMLImageElement;
  const disabledBtn: '' | 'disabled' = canStartFight ? '' : 'disabled';
  const fightBtn: HTMLButtonElement = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
  }) as HTMLButtonElement;

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: [FighterDetails, FighterDetails]): void {
  renderArena(selectedFighters);
}
