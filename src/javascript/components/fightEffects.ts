import { Player } from '../helpers/interfaces';

export function setFighterHealthBar(player: Player): void {
  const percent: number = Math.max(0, (player.health * 100) / player.fighter.health);
  player.indicator.style.width = `${percent}%`;
}

export function playSound(type: 'hit' | 'crit' | 'block' = 'hit'): void {
  let src: string = './resources/hit.mp3';
  if (type === 'crit') src = './resources/crit.mp3';
  if (type === 'block') src = './resources/block.mp3';
  const audio: HTMLAudioElement = new Audio();
  audio.src = src;
  audio.autoplay = true;
}

export function showIconHit(fighter: Player, type: 'hit' | 'crit' | 'block' = 'hit'): void {
  let cls: string = 'arena___health-hit';
  if (type === 'crit') cls = 'arena___health-crit';
  if (type === 'block') cls = 'arena___health-block';
  fighter.indicator.classList.add(cls);
  setTimeout((): void => {
    fighter.indicator.classList.remove(cls);
  }, 200);
}
