import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight } from './fight';
import { showWinnerModal } from './modal/winner';
import { FighterDetails } from '../helpers/interfaces';

type SelectedFighters = [FighterDetails, FighterDetails];

export function renderArena(selectedFighters: SelectedFighters): void {
  const root: HTMLDivElement = document.getElementById('root') as HTMLDivElement;
  const arena: HTMLDivElement = createArena(selectedFighters);

  root.innerHTML = '';
  root.append(arena);

  fight(...selectedFighters).then((winner: FighterDetails): void => {
    showWinnerModal(winner);
  });
}

function createArena(selectedFighters: SelectedFighters): HTMLDivElement {
  const arena: HTMLDivElement = createElement({ tagName: 'div', className: 'arena___root' }) as HTMLDivElement;
  const healthIndicators: HTMLDivElement = createHealthIndicators(...selectedFighters);
  const fighters: HTMLDivElement = createFighters(...selectedFighters);

  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(leftFighter: FighterDetails, rightFighter: FighterDetails): HTMLDivElement {
  const healthIndicators: HTMLDivElement = createElement({
    tagName: 'div',
    className: 'arena___fight-status',
  }) as HTMLDivElement;
  const versusSign: HTMLDivElement = createElement({
    tagName: 'div',
    className: 'arena___versus-sign',
  }) as HTMLDivElement;
  const leftFighterIndicator: HTMLDivElement = createHealthIndicator(leftFighter, 'left');
  const rightFighterIndicator: HTMLDivElement = createHealthIndicator(rightFighter, 'right');

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter: FighterDetails, position: 'right' | 'left'): HTMLDivElement {
  const container: HTMLDivElement = createElement({
    tagName: 'div',
    className: 'arena___fighter-indicator',
  }) as HTMLDivElement;
  const fighterName: HTMLSpanElement = createElement({
    tagName: 'span',
    className: 'arena___fighter-name',
  }) as HTMLSpanElement;
  const indicator: HTMLDivElement = createElement({
    tagName: 'div',
    className: 'arena___health-indicator',
  }) as HTMLDivElement;
  const bar: HTMLDivElement = createElement({
    tagName: 'div',
    className: 'arena___health-bar',
    attributes: { id: `${position}-fighter-indicator` },
  }) as HTMLDivElement;

  fighterName.innerText = fighter.name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(firstFighter: FighterDetails, secondFighter: FighterDetails): HTMLDivElement {
  const battleField: HTMLDivElement = createElement({
    tagName: 'div',
    className: `arena___battlefield`,
  }) as HTMLDivElement;
  const firstFighterElement: HTMLDivElement = createFighter(firstFighter, 'left');
  const secondFighterElement: HTMLDivElement = createFighter(secondFighter, 'right');

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter: FighterDetails, position: 'right' | 'left'): HTMLDivElement {
  const imgElement: HTMLImageElement = createFighterImage(fighter);
  const positionClassName: string = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement: HTMLDivElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
  }) as HTMLDivElement;

  fighterElement.append(imgElement);
  return fighterElement;
}
