import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import { createFighterImage } from '../fighterPreview';
import { FighterDetails } from '../../helpers/interfaces';
import App from '../../app';

export function showWinnerModal(fighter: FighterDetails): void {
  const title: string = `Winner ${fighter.name}`;
  const bodyElement: HTMLDivElement = createFighter(fighter);
  showModal({
    title,
    bodyElement,
    onClose,
  });
}

function createFighter(fighter: FighterDetails): HTMLDivElement {
  const imgElement: HTMLImageElement = createFighterImage(fighter);
  const fighterElement: HTMLDivElement = createElement({
    tagName: 'div',
    className: 'arena___fighter',
  }) as HTMLDivElement;

  fighterElement.append(imgElement);
  return fighterElement;
}

function onClose(): void {
  const arena: Element | null = document.querySelector('.arena___root');
  if (arena && arena.remove) arena.remove();
  new App();
}
