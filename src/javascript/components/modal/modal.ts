import { createElement } from '../../helpers/domHelper';

interface Modal {
  title: string;
  bodyElement: HTMLDivElement;
  onClose: () => void;
}

export function showModal({ title, bodyElement, onClose }: Modal): void {
  const root: HTMLDivElement = getModalContainer();
  const modal: HTMLDivElement = createModal({ title, bodyElement, onClose });

  root.append(modal);
}

function getModalContainer(): HTMLDivElement {
  return document.getElementById('root') as HTMLDivElement;
}

function createModal({ title, bodyElement, onClose }: Modal): HTMLDivElement {
  const layer: HTMLDivElement = createElement({ tagName: 'div', className: 'modal-layer' }) as HTMLDivElement;
  const modalContainer: HTMLDivElement = createElement({ tagName: 'div', className: 'modal-root' }) as HTMLDivElement;
  const header: HTMLDivElement = createHeader(title, onClose);

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title: string, onClose: () => void): HTMLDivElement {
  const headerElement: HTMLDivElement = createElement({ tagName: 'div', className: 'modal-header' }) as HTMLDivElement;
  const titleElement: HTMLSpanElement = createElement({ tagName: 'span' }) as HTMLSpanElement;
  const closeButton: HTMLDivElement = createElement({ tagName: 'div', className: 'close-btn' }) as HTMLDivElement;

  titleElement.innerText = title;
  closeButton.innerText = '×';

  const close = (): void => {
    hideModal();
    onClose();
  };
  closeButton.addEventListener('click', close);
  headerElement.append(titleElement, closeButton);

  return headerElement;
}

function hideModal(): void {
  const modal: Element = document.getElementsByClassName('modal-layer')[0];
  if (modal.remove) modal.remove();
}
