import { createElement } from '../helpers/domHelper';
import { FighterDetails } from '../helpers/interfaces';

export function createFighterPreview(fighter: FighterDetails | false, position: 'right' | 'left'): HTMLDivElement {
  const positionClassName: string = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement: HTMLDivElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  }) as HTMLDivElement;

  if (fighter) {
    const image: HTMLImageElement = createFighterImage(fighter);
    const info: HTMLDivElement = createFighterInfo(fighter);
    fighterElement.append(image, info);
  } else if (fighter === false) {
    const error: HTMLDivElement = createLoadErrorElement('Failed to load fighter 😪');
    fighterElement.append(error);
  }

  return fighterElement;
}

export function createFighterImage(fighter: FighterDetails): HTMLImageElement {
  const attributes: { [key: string]: string } = {
    src: fighter.source,
    title: fighter.name,
    alt: fighter.name,
  };

  const imgElement: HTMLImageElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  }) as HTMLImageElement;

  return imgElement;
}

function createFighterInfo(fighter: FighterDetails): HTMLDivElement {
  const info: HTMLDivElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___info',
  }) as HTMLDivElement;
  const name: HTMLHeadingElement = createElement({
    tagName: 'h2',
    className: 'fighter-preview___info-name',
  }) as HTMLHeadingElement;
  name.innerText = fighter.name;
  const health: HTMLDivElement = createFighterInfoElement('❤️ Health', fighter.health) as HTMLDivElement;
  const attack: HTMLDivElement = createFighterInfoElement('⚔️ Attack', fighter.attack) as HTMLDivElement;
  const defense: HTMLDivElement = createFighterInfoElement('🛡️ Defense', fighter.defense) as HTMLDivElement;
  info.append(name, health, attack, defense);
  return info;
}

function createFighterInfoElement(name: string, value: number): HTMLDivElement {
  const element: HTMLDivElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___info-row',
  }) as HTMLDivElement;
  const nameElement: HTMLSpanElement = createElement({ tagName: 'span' }) as HTMLSpanElement;
  nameElement.innerText = `${name}:`;
  const valueElement: HTMLSpanElement = createElement({ tagName: 'span' }) as HTMLSpanElement;
  valueElement.innerText = String(value);
  element.append(nameElement, valueElement);
  return element;
}

function createLoadErrorElement(textError: string): HTMLDivElement {
  const element: HTMLDivElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___info fighter-preview___load-error',
  }) as HTMLDivElement;
  element.innerText = textError;
  return element;
}
