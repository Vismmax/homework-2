import { createFighters } from './components/fightersView';
import { fighterService } from './services/fightersService';
import { Fighter } from './helpers/interfaces';

class App {
  constructor() {
    this.startApp();
  }

  static rootElement: HTMLDivElement = document.getElementById('root') as HTMLDivElement;
  static loadingElement: HTMLDivElement = document.getElementById('loading-overlay') as HTMLDivElement;

  async startApp(): Promise<void> {
    try {
      App.loadingElement.style.visibility = 'visible';

      const fighters: Fighter[] = await fighterService.getFighters();
      const fightersElement: HTMLDivElement = createFighters(fighters);

      App.rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }
}

export default App;
