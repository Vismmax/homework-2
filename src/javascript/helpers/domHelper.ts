import { CustomElement } from './interfaces';

export function createElement({ tagName, className, attributes = {} }: CustomElement): HTMLElement {
  const element: HTMLElement = document.createElement(tagName);

  if (className) {
    const classNames: string[] = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach((key: string): void => element.setAttribute(key, attributes[key]));

  return element;
}
