import { fightersDetails, fighters } from './mockData';
import { Fighter, FighterDetails } from './interfaces';

const API_URL: string =
  'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
const useMockAPI: boolean = true;

interface Result {
  content: string,
  download_url: string,
  encoding: string,
  git_url: string,
  html_url: string,
  name: string,
  path: string,
  sha: string,
  size: number,
  type: string,
  url: string,
  _links: {
    git: string,
    html: string,
    self: string
  }
}

async function callApi(
  endpoint: string,
  method: 'GET' | 'POST' | 'PUT' | 'DELETE'
): Promise<Fighter[] | FighterDetails> {
  const url: string = API_URL + endpoint;
  const options: { method: string } = {
    method,
  };

  return useMockAPI
    ? fakeCallApi(endpoint)
    : fetch(url, options)
        .then(
          (response: Response): Promise<Result> =>
            response.ok ? response.json() : Promise.reject(Error('Failed to load'))
        )
        .then(
          (result: Result): Promise<Fighter[] | FighterDetails> =>
          JSON.parse(atob(result.content))
        )
        .catch((error: Error): never => {
          throw error;
        });
}

async function fakeCallApi(endpoint: string): Promise<Fighter[] | FighterDetails> {
  const response: Fighter[] | FighterDetails | undefined =
    endpoint === 'fighters.json' ? fighters : getFighterById(endpoint);

  return new Promise((resolve, reject) => {
    setTimeout((): void => (response ? resolve(response) : reject(Error('Failed to load'))), 500);
  });
}

function getFighterById(endpoint: string): FighterDetails | undefined {
  const start: number = endpoint.lastIndexOf('/');
  const end: number = endpoint.lastIndexOf('.json');
  const id: string = endpoint.substring(start + 1, end);

  return fightersDetails.find((it: FighterDetails) => it._id === id);
}

export { callApi };
