export interface Fighter {
  _id: string;
  name: string;
  source: string;
}

export interface FighterDetails {
  _id: string;
  name: string;
  source: string;
  health: number;
  attack: number;
  defense: number;
}

// Можно так, но предыдущее нагляднее
// export interface FighterDetails extends Fighter{
//   health: number,
//   attack: number,
//   defense: number,
// }

export interface Player {
  fighter: FighterDetails;
  health: number;
  blockCriticalHit: boolean;
  indicator: HTMLDivElement;
}

export interface CustomElement {
  tagName: string;
  className?: string;
  attributes?: {
    [index: string]: string;
  };
}
